(ns scratch
  (:use [clojure.test]))

(defn sliding-steps [nums]
  (->> (partition 3 1 nums)
       (map #(apply + %))
       (count-steps-up)))

(defn count-steps-up [nums]
  (let [step-up? (fn [[a b]] (< a b))]
    (->> (partition 2 1 nums)
         (filter step-up?)
         (count))))


(let [nums (load-file "input.edn")]
  (println (count-steps-up nums))
  (println (sliding-steps nums)))

(deftest tests
  (are [nums cnt] (= (count-steps-up nums) cnt)
       [] 0
       [1 1 1] 0
       [0] 0
       [1 2 3 3 2 1] 2 
       [1 2 3 3 2 1 2 3] 4 
       [3 3 4 5 3 3 2 3 3 20 20 19 13 20] 5
       ))

(run-tests)
