(ns scratch)

(def x 0)
(def y 0)

(defn down [a n]
  (+ a n))

(defn up[a n]
  (- a n))

(defn forward[a n]
  (def y (+ y (* a n)))
  (def x (+ x n))
  a)

(load-file "input.clj")
(* x y)
