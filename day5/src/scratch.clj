(ns day5
  (:require [clojure.edn :as edn]))

(def coords (edn/read-string (slurp "input.edn")))
(comment 
  (def coords (partition 2 [ [0,9] [5,9] [8,0] [0,8] [9,4] [3,4] [2,2] [2,1] [7,0] [7,4] [6,4] [2,0] [0,9] [2,9] [3,4] [1,4] [0,0] [8,8] [5,5] [8,2]])))

(defn line-type [[[x1 y1] [x2 y2]]]
  (cond
    (= x1 x2) :vertical
    (= y1 y2) :horizontal
    :else :diagonal))

(defn expand-line [coords]
  (let [[[x1 y1] [x2 y2]] (sort coords)
        ranges (case (line-type coords)
                 :vertical   [(repeat x1) (range y1 (inc y2))]
                 :horizontal [(range x1 (inc x2)) (repeat y1)]
                 :diagonal   [(range x1 (inc x2)) (iterate (if (> y2 y1) 
                                                             inc 
                                                             dec) y1)])]
     (apply map vector ranges)))
           
;;--- part1 ---
;;(def coords (filter #(#{:horizontal :vertical} (line-type %)) coords))

;;--- part2 ---
(->> coords 
     (mapcat expand-line)
     (frequencies)
     (map val)
     (filter #(> % 1))
     count)




