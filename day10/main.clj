(ns day10
  (:require [clojure.string :as str]))

(def lines (line-seq (clojure.java.io/reader "input.edn")))

(defn remove-good-chunks
  "Recursively removes all pairs of closed brackets. 
  For malformed lines returns a non-empty string with at least one closing bracket remaining.
  For incomplete lines returns a non-empty string without any closing brackets.
  For well formatted complete lines returns empty string."
  [s]
  (let [sn (str/replace s #"\{\}|\[\]|\(\)|\<\>" "")]
    (if (not= sn s)
      (recur sn)
      sn)))

(comment
  (remove-good-chunks "({}[>)") ;=> "([>)"
  (remove-good-chunks "({<>") ;=> "({"
  (remove-good-chunks "({<>})") ;=> ""
  )

(defn score-line1 [bad-chunks]
  (let [scores {\) 3 \] 57 \} 1197 \> 25137}
        first-bad-closing (some (set (keys scores)) bad-chunks)]
    (or (scores first-bad-closing)
        0)))

(defn score-line2 [bad-chunks]
  (if (zero? (score-line1 bad-chunks))
    (let [line-scores (map {\( 1 \[ 2 \{ 3 \< 4} (reverse bad-chunks))]
      (reduce #(+ (* 5 %1) %2) 0 line-scores))
    0))

(def bad-chunk-lines (map remove-good-chunks lines))

(reduce + (map score-line1 bad-chunk-lines)) ;=> 415953 

(defn median [coll]
  (let [v-sorted (vec (sort coll))
        mid-idx (quot (count v-sorted) 2)]
    (v-sorted mid-idx)))

(median (filter (complement zero?)
                (map score-line2 bad-chunk-lines))) ;=>2292863731

(comment
  (def lines (str/split-lines
"[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]"))

(def bad-chunk-lines (map remove-good-chunks lines))
(reduce + (map score-line1 bad-chunk-lines))
(median (filter (complement zero?)
                  (map score-line2 bad-chunk-lines)))
)
