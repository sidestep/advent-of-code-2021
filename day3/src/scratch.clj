(ns scratch
  (:require [clojure.edn :as edn]))

(comment (def bits 
[[0 0 1 0 0]
[1 1 1 1 0]
[1 0 1 1 0]
[1 0 1 1 1]
[1 0 1 0 1]
[0 1 1 1 1]
[0 0 1 1 1]
[1 1 1 0 0]
[1 0 0 0 0]
[1 1 0 0 1]
[0 0 0 1 0]
[0 1 0 1 0]]))

(def bits (edn/read-string (slurp "input.edn")))

;; ------ part one -------
(defn bit-mask->int [mask]
  (->> (reverse mask)
       (keep-indexed #(when (= 1 %2) %1))
       (reduce bit-set 0)))

(let [bit-counts (apply map (fn [& col](frequencies col)) bits)
      gamma (map #(if (> (% 1) (% 0)) 1 0) bit-counts)
      epsilon (map inc gamma)] ;; to turn zeros to ones
  (* (bit-mask->int gamma) (bit-mask->int epsilon)))

;; ------ part two -------

(defn pick-partition[fcomp {zeros 0 ones 1}]
 (if (fcomp (count ones) (count zeros))
    ones
    zeros))

(defn find-bits[bits fpartition-selector]
  (loop [b bits n 0]
    (if (> (count b) 1)
      (recur (fpartition-selector (group-by #(nth % n) b)) (inc n))
      (first b))))

(let [o2gen (find-bits bits (partial pick-partition >=))
      co2scrub (find-bits bits (partial pick-partition <))]
  (* (bit-mask->int o2gen) (bit-mask->int co2scrub)))
