(ns day4 
  (:require [clojure.edn :as edn])
  (:use clojure.set))

(let [input (edn/read-string (slurp "input.edn"))]
  (def all-nums (first input))
  (def all-boards (partition 5 (rest input))))

(defn bingo-row? [nums board]
  (first
    (keep-indexed #(when (subset? (set %2) (set nums)) %1) board)))

(defn transpose [matrix]
  (apply mapv vector matrix))

(defn bingo? [nums board]
  (or (bingo-row? nums board)
      (bingo-row? nums (transpose board))))

(defn board-score [nums board]
  (* (last nums) 
     (->> (flatten board)
          (filter (complement (set nums)))
          (reduce +))))

(let [game-nums (for [i (range 5 (count all-nums))] 
                  (subvec all-nums 0 i))
      winners (for [nums game-nums 
                    board all-boards :when (bingo? nums board)]
                [nums board])
      [win-nums win-board] (first winners)]
      (board-score win-nums win-board))

;; ------ part two -------

(defn winners [nums boards]
  (filter #(bingo? nums %) boards))

(def all-winners (winners all-nums all-boards))

(let [[-cnt prev-winners] 
  (loop [-cnt 1]
    (let [wins (winners (drop-last -cnt all-nums) all-boards)]
      (if (= all-winners wins)
        (recur (inc -cnt))
        [-cnt wins])))
  last-winner (first (difference (set all-winners) (set prev-winners)))]
  (board-score (drop-last (dec -cnt) all-nums) last-winner))

